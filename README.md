# Intro

This repository contains two small examples which show how to use the Birmingham GPUs in python.
* The example in the folder `GPU` performs a log likelihood fit using iminuit in many bins. The potential speed up using GPUs is shown. It runs in a jupyter notebook.
* The example in the folder `ML` shows how to train a keras neural network on GPUs. It runs in the shell.

# Prerequisites

The `ML` example requires ROOT6 with python3 support (Note: skip this if you are only interested in the `GPU` example)
If not already done, build it from source like this:

```
cd ~
mkdir install; cd $_
mkdir root6python3; cd $_
git clone http://github.com/root-project/root.git source
cd source
git checkout -b v6-19-01 v6-19-01 #other/newer version?
cd ..
mkdir build; cd $_
cmake ../source -DPYTHON_EXECUTABLE=/usr/bin/python3
cmake --build .
```

If you already have it installed somewhere else, change the following line in `ML/setup.sh` accordingly:

```
 . ~/install/root6python3/build/bin/thisroot.sh
```

# Common Setup

Login onto the uni computing facilities as usual (replace `tf` with your initials):

```
ssh -Y tf@eprexA.ph.bham.ac.uk
```

From there, login onto the machines with GPUs:

```
ssh -Y $USER@epldt001
```

Clone this repository somewhere:

```
git clone https://gitlab.cern.ch/tofitsch/gpuatbirmingham.git gpuAtBirmingham
```

Save it's path as `$GPUATBHAMdir` into your `~/.bashrc`, for example by putting the following line into it:

```
export GPUATBHAMdir=/home/$USER/work/gpuAtBirmingham
```

Source your `~/.bashrc` to apply the change:

```
. ~/.bashrc
```

setup cc7:

```
exec /usr/cc7/bash
```

Create a virtual environment:

```
cd $GPUATBHMdir
python3 -m venv env
```

Activate the environment and choose the right cuda version (do this after each new login):

```
. GPU/setup.sh
```

We are using pip as package manager in the following. Upgrade it:

```
pip install --upgrade pip
```

Install the packages needed for the `GPU` example:

```
pip install -r GPU/requirements.txt
```
# Run the GPU example

To run locally within a jupyter-notebook:

```
jupyter-notebook GPU/logLikelihood.ipynb
```

If you are not physically working on your computer at Birmingham but are connected remotely via ssh, this will be very slow. In that case you should rather run it remotely:

# Working remotely with jupyter

Put the following into you local (e.g. your laptop) `~/.ssh/config` file (change tf into your initials):

```
Host unibroker
Hostname eprexA.ph.bham.ac.uk
User tf

Host unigpu
ProxyCommand ssh -Y unibroker nc epldt001 22
User tf
```

Now you can directly login onto the GPU machines with:

```
ssh -Y unigpu
```

There, activate the environment as usual:

```
. GPU/setup.sh
```

and start the jupyter notebook (choose a password and replace port 9000 with a free port (9000 should be fine, though)):

```
jupyter-notebook password
jupyter-notebook --port=9000 --no-browser &
```

On your local machine (e.g. your laptop) (replace 9000 with the port you have chosen above, and 7777 with a free port on your machine (again, 7777 should already by fine)):

```
ssh -N -f -L 7777:localhost:9000 unigpu
```

open browser on laptop, type `localhost:7777` into the address bar, and enter the password which you have defined above.

Now you should be able to run the notebook located in `GPU/logLikelihood.ipynb` from within your browser.
Make sure to click on `Kernel->Restart and Clear Output` before doing so, and then run it with `Cells->Run All`

Note: To free up ports on your local machine once you're done (only necessary if you won't restart), get their ids with:

```
ps aux | grep unigpu
```

and kill the processes with:

```
kill <id>
```

Or to kill all of your ssh sessions at once:

```
killall ssh
```

To free up ports on the remote GPU machine (you could also just log out and in again), get their ids with:

```
ps aux | grep jupyter
```

and kill the processes with:

```
kill <id>
```

or

```
killall jupyter-notebook
```

# Notes

To show the available GPUs and monitor their current usage:

```
watch -n 1 nvidia-smi
```

There are two Nvidia Tesla P100 GPUs

# Setup for the ML example

Download CuDNN from here: https://developer.nvidia.com/cudnn
You will have to sign up for `NVIDIA developer` and answer some nosy questions.

For the cuda version installed at Birmingham, choose this option:

* Download cuDNN v8.0.3 (August 26th, 2020), for CUDA 11.0
** cuDNN Library for Linux (x86_64)

(do not choose the [Power] version)

Make the folder `~/install/cudnn` and copy the `*.tgz` you just downloaded there.
Unpack it:

```
tar xvzf cudnn-*
```

Add the libraries to your environmental variables (These lines are already included in `ML/setup.sh`):

```
export CPATH=$CPATH:~/install/cudnn/cuda/include
export PATH=$PATH:~/install/cudnn/cuda/include
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:~/install/cudnn/cuda/lib64
```

Install keras, tensorflow, and other requirements:

```
pip install -r ML/requirements.txt
```

To install tensorflow 2.3.0 (latest stable version) with cuda11 support instead of the nightly build specified in `requirements.txt`, run this afterwards:

```
wget https://github.com/davidenunes/tensorflow-wheels/releases/download/r2.3.cp38.gpu/tensorflow-2.3.0-cp38-cp38-linux_x86_64.whl -O tf2.3.0-cuda11.whl
pip install tf2.3.0-cuda11.whl
```

# Run the ML example

Before you run the example, create a Ntuple with randomly generated data:

```
python ML/generateRandomNtuple.py
```

Then run the keras example which trains on the data in that Ntuple:

```
python ML/train.py
```

Ignore the warnings about missing `TensorRT` libraries. They are optional. Might speed up the training process though. Read more about them [here](https://developer.nvidia.com/tensorrt).

If everything goes well, it will create two `.png` files with the network predictions, and the loss during training.
