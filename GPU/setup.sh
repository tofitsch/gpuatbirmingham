#!/bin/bash

if [ -d "/usr/cc7" ]; then
 echo "cc7 has not been setup. Setting it up now and exeting. Rerun this setup script afterwards"
 exec /usr/cc7/bash
fi

if [[ `hostname` == "epldt"* ]]; then

 # python environment
 . env/bin/activate

 # pycuda
 export LD_LIBRARY_PATH=/usr/local/gcc-7/lib:$LD_LIBRARY_PATH
 export PATH=/usr/local/cuda-11.0/bin:/usr/local/gcc-7/bin:$PATH
 export CPATH=$CPATH:~/install/cudnn/cuda/include
 export PATH=$PATH:~/install/cudnn/cuda/include
 export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:~/install/cudnn/cuda/lib64:/usr/local/cuda-10.1/lib64

 # pyopencl
 export PYOPENCL_CTX=0 # choose 'CUDA' as platform for opencl

 echo "setup done"
else
 echo "not on GPU machine! exiting."
fi
