#!/bin/bash

if [ -d "/usr/cc7" ]; then
 echo "cc7 has not been setup. Setting it up now and exeting. Rerun this setup script afterwards"
 exec /usr/cc7/bash
fi

if [[ `hostname` == "epldt"* ]]; then
 . env/bin/activate
 . ~/install/root6python3/build/bin/thisroot.sh
 export LD_LIBRARY_PATH=/usr/local/gcc-7/lib:$LD_LIBRARY_PATH
 export PATH=/usr/local/cuda-10.1/bin:/usr/local/gcc-7/bin:$PATH
 export CPATH=$CPATH:~/install/cudnn/cuda/include
 export PATH=$PATH:~/install/cudnn/cuda/include
 export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:~/install/cudnn/cuda/lib64:/usr/local/cuda-10.1/lib64
 GPUchoice=$1
 if [[ "$GPUchoice" != "0" ]] && [[ "$GPUchoice" != "1" ]]; then
  echo "No valid GPU chosen (has to be either '0' or '1'). Using '0' by default"
  GPUchoice="0"
 else
  echo "GPU $GPUchoice chosen"
 fi
 export CUDA_VISIBLE_DEVICES=$GPUchoice # choose GPU for tensorflow
 export PYOPENCL_CTX=0 # choose 'CUDA' as platform for opencl
# export PYOPENCL_CTX="0:$GPUchoice" # choose GPU for opencl, the '0' means 'CUDA' platform. Only used if CUDA_VISIBLE_DEVICES is not set
 echo "setup done"
else
 echo "not on GPU machine! exiting."
fi
