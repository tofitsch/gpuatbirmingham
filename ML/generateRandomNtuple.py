from numpy import random
from array import array
from ROOT import TFile, TTree

# options
outFileName = 'exampleData.root'
samples = {'sig': 1000, 'bkg': 10000}
nBranches = 3

# fill Ntuple randomly
tree = dict()
branch = []
mu = []
sigma = []

print('gernerating random Ntuple "{}"'.format(outFileName))

outFile = TFile(outFileName, 'RECREATE')

for s, sample in enumerate(samples):

  tree[sample] = TTree(sample, sample)

  branch.append([])
  mu.append([])
  sigma.append([])

  for b in range(nBranches):

    branch[s].append(array('f', [0.]))
    mu[s].append(random.normal(0., 1.))
    sigma[s].append(random.uniform(.5, 1.))

    print(' > gaussian with N: {} \t mu: {:1.2}\t sigma: {:1.2}\t for "var{}" in sample "{}"'.format(samples[sample],  mu[s][b], sigma[s][b], b, sample))

    tree[sample].Branch('var'+str(b), branch[s][b], 'var'+str(b)+'/F')

for s, sample in enumerate(samples):

  for i in range(samples[sample]):

    for b in range(nBranches):

      branch[s][b][0] = random.normal(mu[s][b], sigma[s][b])

    tree[sample].Fill()

  tree[sample].Write()

outFile.Close()
