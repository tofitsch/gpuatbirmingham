from keras.models import Sequential
from keras.layers import Dense
from sklearn.model_selection import train_test_split
import uproot
import numpy as np
import matplotlib.pyplot as plt

inFileName = 'exampleData.root'
branchNames = ['var0', 'var1', 'var2']
labeling = {'sig': 1, 'bkg': 0}

# load the data as numpy arrays
inputs, labels = dict(), dict()
for sample, label in labeling.items():
  tree = uproot.open(inFileName)[sample]
  inputs[sample] = np.stack([branch for key, branch in tree.arrays(branchNames).items()], axis=0).T
  labels[sample] = np.full(tree.numentries, label, dtype=int)

# merge sig and bkg and split into test and train
inputs['all'] = np.concatenate((inputs['sig'], inputs['bkg']))
labels['all'] = np.concatenate((labels['sig'], labels['bkg']))
inputs['train'], inputs['test'], labels['train'], labels['test'] = train_test_split(inputs['all'], labels['all'], test_size=0.5)

# retrive all sig events in the train sample etc.
for sample in ['sig', 'bkg']:
  for role in ['train', 'test']:
    inputs[role + '_' + sample] = np.stack([x for i, x in enumerate(inputs[role]) if labels[role][i] == labeling[sample]])
print('shapes:')
[print(' > inputs', key, arr.shape) for key, arr in inputs.items()]
[print(' > labels', key, arr.shape) for key, arr in labels.items()]

# define the model (2 hidden layers with 8 nodes each, one output layer with 1 node)
nInputs = inputs['all'].shape[1]
model = Sequential()
model.add(Dense(8, input_dim=nInputs, activation='relu'))
model.add(Dense(8, activation='relu'))
model.add(Dense(1, activation='sigmoid'))

# compile the model
model.compile(loss='binary_crossentropy', optimizer='adam')

# train the model
history = model.fit(inputs['train'], labels['train'], validation_data=(inputs['test'], labels['test']), epochs=100, batch_size=int(2e2))

# plot loss
lossTrain = history.history['loss']
lossTest = history.history['val_loss']
x = range(len(lossTrain))
plt.plot(x, lossTrain, alpha=.5, color='g', label='train')
plt.plot(x, lossTest, alpha=.5, color='m', label='test')
plt.legend(loc='upper right')
plt.xlabel('epoch')
plt.ylabel('loss')
plt.savefig('loss.png')
plt.clf()

# plot predictions
predictions = dict()
for sample in ['sig', 'bkg']:
  for role in ['train', 'test']:
    key = role + '_' + sample
    predictions[key] = model.predict(inputs[key], verbose=1)
    hist, bins = np.histogram(predictions[key], range=(0., 1.), bins=10)
    hist = np.array(hist, dtype=np.float64)
    hist /= sum(hist)
    binCentres = (bins[:-1] + bins[1:])/2.
    plt.hist(bins[:-1], bins, weights=hist, label=key, alpha = 0.5, color=('red' if sample == 'sig' else 'blue'), hatch=('//' if role == 'test' else '\\'))
    plt.legend(loc='upper right')
    plt.savefig('predictions.png')
